<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::get('test/{id}/{name}', function($id, $name){
    return 'Test '.$id.'=>'.$name;
})->where(['id' => '[0-9]+', 'name'=>'[a-z,A-Z]+']);

 Route::controllers([
 	'user' => 'Auth\AuthController',
 	'password' => 'Auth\PasswordController',
 ]);
